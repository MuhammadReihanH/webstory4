from django.urls import include, path
from .views import contact

urlpatterns = [
    path('', contact, name='contact'),
]
